# Tkinter calculator

## About

This is a calculator application written in Python with a graphic user interface
created using tkinter from the Python standard library.

Some notable features about this application:

* Custom equation parsing and solving algorithms
* Supports long expressions with nested parentheses
* Has dark and light themes that can be changed on-the-fly
* Adjustable font-size and calculation precision
* Full keyboard support with useful shortcuts

![dark themed calculator](docs/screenshots/dt-resized.png "Dark theme")
![light themed calculator](docs/screenshots/lt-resized.png "Light theme")

The design of this calculator was inspired by the excellent
[GNOME Calculator](https://wiki.gnome.org/Apps/Calculator).

## Compatibility

This program is fully compatible with Python 3.9 on Linux.

Please see the documentation for details concerning other:

* [Python versions](docs/README.md#Python)
* [Operating systems](docs/README.md#Operating-systems)

## Installation

Instructions are available for installing and using this application
through a:

* [GUI file manager](docs/README.md#GUI-file-manager)
* [Terminal](docs/README.md#Terminal)

## Design discussion

There are numerous areas where the structure of the code, its formatting, and its
overall design could be improved. [Here](docs/design-discussion.md) are some tips,
tricks, and lessons-learned, for anyone interested in why certain choices were or
were not made during the development of this project.

## Contributing

Please report any suggestions or bugs to the
[GitLab issue tracker](https://gitlab.com/emerac/calculator-tkinter/-/issues). See
[CONTRIBUTING](CONTRIBUTING.md) for more details.

## License

This program is free software and is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.

Copyright © 2021 emerac
