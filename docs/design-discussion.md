# Design discussion

## About

There are **numerous** places where improvements can be made since this a first
attempt at full-on application development. Presented here is a discussion of the
reasons behind why some design choices were made in the hopes that it will help
contributers or other developers interested in making a similar project.

## Motivation

The goal of this project is to gain experience creating a complete application. As
such, `eval()` and PyParsing were eschewed in favor of creating a custom equation
parsing and solving algorithm.

## Tips, tricks, and lessons learned

### Use object-oriented programming (OOP)

If ever there was any uncertainty over when to use OOP, this project pretty-well
fixed that. By using OOP, the structure of the project would have been much more
organized and it would resolved a number of other smaller issues.

By setting up the application as a class and then instantiating frame, text,
and button objects within, attributes of those objects could be changed very easily.
The lack of classes is felt most strongly in two places:

1. The global variable `precision` was needed so that changes to the precision would
take effect, well, globablly.
2. Similarly, `ans` is a global variable because subsequent calculations needed
to reference it without it being explicitly set in a calculation.

Global variables have their place, but they lead to a lot of confusion because it
is hard to tell when and where they are being changed.

### Split code up into different modules

If instead of one large module, the program was broken up into smaller modules, the
structure of the project would have been much clearer. One could argue, too, that
splitting code up into separate modules encourages writing code with reusability in
mind since each function or class is not inherently tied to one setting.

### Where practical, try to write pure functions

Pure functions are great for unit testing because you can always be sure the
function will behave predictably. In this case, the expression list is passed
around to multiple functions and it becomes difficult to keep track of what is
coming into the function and what is coming out.

### Make return types consistent

In the function that handles multiplication and division operations, it sometimes
returns the solved list, other times it returns an error. This makes testing more
complicated and can necessitate some strange conditionals to handle the inconsistent
types.

### Give test-driven development a try

Test-driven development seems like a really useful concept because it forces the
developer to think in advance about what the function should do. If writing
the program was approached from this angle, functions would have been shorter
and easier to debug. Additionally, many special cases would have gotten caught and
incorporated into the design from the beginning rather than at the end.
