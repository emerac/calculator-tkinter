#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A simple calculator with a tkinter graphic user interface.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import copy
import tkinter as tk
import tkinter.ttk as ttk
from typing import Any
from typing import Optional
from typing import Union


def add_missing_zeros(expression: list[Any]) -> list[Any]:
    """Add zeros to items before or after the decimal point.

    Parameters
    ----------
    expression : list of str, int, or float
        A math expression separated into terms.

    Returns
    -------
    expression : list of str, int, or float
        A math expression separated into terms.

    """
    for index, term in enumerate(expression):
        if term.startswith("."):
            expression[index] = "0" + term
        elif term.endswith("."):
            expression[index] = term + "0"
    return expression


def change_window_geometry(option: str) -> None:
    """Change the size and location of the primary window.

    Parameters
    ----------
    option : str
        Window geometry choice made by user.

    Returns
    -------
    None

    """
    max_width: int = root.winfo_screenwidth()
    max_height: int = root.winfo_screenheight()
    new_width: str = str(int(0.5 * max_width))
    if option == "Default":
        root.geometry("540x408+100+100")
        root.geometry("+100+100")
    elif option == "Maximize":
        root.geometry("1920x1080")
    elif option == "Snap Left":
        root.geometry(f"{new_width}x{max_height}+0+0")
    elif option == "Snap Right":
        root.geometry(f"{new_width}x{max_height}+{new_width}+0")


def check_minuses(sub_terms: list[Any]) -> Any:
    """Determine if the minus signs in an expression are valid syntax.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.
    exception : SyntaxError
        Indicates that a minus sign was the cause of invalid syntax.
        The exception is returned if there is an error. Otherwise,
        sub_terms is returned.

    """
    for index, term in enumerate(sub_terms):
        if len(sub_terms) > 1 and index != len(sub_terms) - 1:
            if term == "-" and sub_terms[index + 1] in [
                "/", "*", "+", "²", "%",
            ]:
                return SyntaxError
    return sub_terms


def clear_entire_display() -> None:
    """Clear display and print a new prompt.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    display["state"] = "normal"
    display.delete("1.0", "end")
    display_prompt()
    display["state"] = "disabled"
    display.see("end")
    mainframe.focus_set()


def clear_expression() -> None:
    """Delete the contents of the current line.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    display["state"] = "normal"
    display.delete("end - 1 lines", "end")
    if display.index("end - 1 char")[0] != "1":
        # This ensures previous lines cannot be deleted, too.
        display.insert("end", "\n")
    display_prompt()
    display["state"] = "disabled"
    mainframe.focus_set()


def close_precision_dialog(window: tk.Toplevel, setting: tk.StringVar) -> None:
    """Update the precision and close the dialog window.

    Parameters
    ----------
    window : tk.Toplevel
        The precision dialog window.
    setting : tk.StringVar
        The precision value chosen by the user.

    Returns
    -------
    None

    """
    global precision
    precision = int(setting.get())
    window.destroy()


def convert_to_number(expression: list[Any]) -> list[Any]:
    """Convert strings representing numbers to int or float types.

    Parameters
    ----------
    expression : list of str
        A math expression separated into terms.

    Returns
    -------
    None

    Raises
    ------
    ValueError
        Used to check if the value can be converted to int or float.

    """
    for index, term in enumerate(expression):
        try:
            expression[index] = int(expression[index])
        except ValueError:
            try:
                expression[index] = float(expression[index])
            except ValueError:
                continue
    return expression


def delete_char() -> None:
    """Delete the last character entered.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    if display.get("end - 1 lines", "end") == "⮞   \n":
        return
    display["state"] = "normal"
    # Tk always puts a newline char at the end, so the actual last
    # character is two spaces before the end.
    display.delete("end - 2 chars")
    display["state"] = "disabled"
    mainframe.focus_set()


def display_answer(answer: Union[int, float]) -> None:
    """Display the answer and move to the next display line.

    Parameters
    ----------
    answer : int or float
        The result of the calculations.

    Returns
    -------
    None

    """
    global precision
    display["state"] = "normal"
    # String formatting is used to specify the number of digits that
    # follow the decimal. Any number of digits are allowed to be
    # before the decimal.
    display.insert(
        "end", "\n" + "= " + "{:.{}f}".format(answer, precision) + "\n\n"
    )
    display_prompt()
    display["state"] = "disabled"
    display.see("end")
    mainframe.focus_set()


def display_error(error: str) -> None:
    """Display the error and move to the next display line.

    Parameters
    ----------
    error : str
        An error code to be displayed

    Returns
    -------
    None

    """
    display["state"] = "normal"
    display.insert("end", "\n" + "Error: " + error + "\n\n")
    display_prompt()
    display["state"] = "disabled"
    display.see("end")
    mainframe.focus_set()


def display_prompt() -> None:
    """Display a prompt to the user that indicates the current line.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    display["state"] = "normal"
    display.insert("end", "⮞   ")
    display["state"] = "disabled"


def evaluate_add_and_sub(sub_terms: list[Any]) -> list[Any]:
    """Solve instances of addition or subtraction in the expression.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    """
    while "+" in sub_terms or "-" in sub_terms:
        for index, term in enumerate(sub_terms):
            if term == "+":
                # Store result in left-most term for easier deletion.
                sub_terms[index - 1] = (
                    sub_terms[index - 1] + sub_terms[index + 1]
                )
                # Right-to-left deletion ensures no change in indexes.
                del sub_terms[index + 1]
                del sub_terms[index]
                # Start over so that the indexes are updated.
                break
            if term == "-":
                # Right-to-left deletion ensures no change in indexes.
                sub_terms[index - 1] = (
                    sub_terms[index - 1] - sub_terms[index + 1]
                )
                # Start over so that the indexes are updated.
                del sub_terms[index + 1]
                del sub_terms[index]
                break
    return sub_terms


def evaluate_expression(expression: list[Any]) -> Any:
    """Solve a mathematical expression.

    Parameters
    ----------
    expression : list of str, int, or float
        A math expression separated into terms.

    Returns
    -------
    expression[0] : int or float
        The answer to the expression if there is no error.
    exception : SyntaxError, ValueError, ZeroDivisionError
        The exception describing what kind of error occurred. If an
        error ocurred, expression[0] is not returned.

    Notes
    -----
    The expression is parsed for the innermost group of parentheses.
    The sub-expression within that group is solved. The result then
    replaces the sub-expression and its parentheses in the original
    expression.

    The function is recursively called until there are no more groups
    of parentheses and the answer is found.

    Example
    -------
    The expression below is evaluated in this manner:

        9+(5*(4/2))
        9+(5*2)
        9+10
        19

    >>> evaluate_expression(
    ...     [9, "+", "(", 5, "*", "(", 4, "/", 2, ")", ")"]
    ... )
    19.0

    """
    while len(expression) > 1:
        left_edge: int = 0
        right_edge: int = 0
        sub_terms: list[Any] = []

        # This obviates the need for a special case when no parens.
        if expression[0] != "(":
            expression.insert(0, "(")
        if expression[len(expression) - 1] != ")":
            expression.append(")")

        # Locate the innermost pair of parentheses.
        for index, term in enumerate(expression):
            if term == "(":
                left_edge = index
            # The first closing paren always marks the innermost pair.
            if term == ")":
                right_edge = index
                break

        # Save the expression in the innermost pair of parentheses.
        if right_edge > 0:
            for index, term in enumerate(expression):
                if index >= left_edge and index <= right_edge:
                    sub_terms.append(term)
            # The first and last items are unneeded parentheses.
            del sub_terms[len(sub_terms) - 1]
            del sub_terms[0]
        else:
            sub_terms = copy.deepcopy(expression)

        # Setup the sub-expression for solving.
        sub_terms = check_minuses(sub_terms)
        if sub_terms == SyntaxError:
            return SyntaxError
        sub_terms = parse_minuses(sub_terms)

        # Solve the sub-expression according to the standard order of
        # operations (PEMDAS).
        sub_terms = evaluate_percents(sub_terms)
        sub_terms = evaluate_root_and_exp(sub_terms)
        if sub_terms == ValueError:
            return ValueError
        sub_terms = evaluate_mult_and_div(sub_terms)
        if sub_terms == ZeroDivisionError:
            return ZeroDivisionError
        sub_terms = evaluate_add_and_sub(sub_terms)

        # Replace the expression and its parentheses with answer.
        expression[left_edge] = sub_terms[0]
        # Find which indexes sub-term occupied so they can be deleted.
        deletion_indices: list[int] = [
            index for index in range(left_edge + 1, right_edge + 1)
        ]
        # Loop backwards so indexes do not change when item is deleted.
        for index in range(len(expression) - 1, -1, -1):
            if index in deletion_indices:
                del expression[index]

        # A group of nested parentheses is solved each function call.
        evaluate_expression(expression)
    # The answer to the entire expression is last item in the list.
    return expression[0]


def evaluate_mult_and_div(sub_terms: list[Any]) -> Any:
    """Solve instances of multiplication or division in the expression.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.
    exception : ZeroDivisionError
        An exception showing that a divide-by-zero situation occurred.
        If there is no error, sub_terms is returned.

    """
    # Interpret numbers that are side-by-side as being multiplied.
    for index in range(len(sub_terms) - 1, -1, -1):
        if index != 0:
            try:
                int(sub_terms[index])
                int(sub_terms[index-1])
                sub_terms.insert(index, "*")
            except ValueError:
                continue

    # Solve normally unless the expression reduces to divide-by-zero.
    try:
        while "*" in sub_terms or "/" in sub_terms:
            for index, term in enumerate(sub_terms):
                if term == "*":
                    sub_terms[index - 1] = (
                        sub_terms[index - 1] * sub_terms[index + 1]
                    )
                    # Right-to-left deletion ensures no index errors.
                    del sub_terms[index + 1]
                    del sub_terms[index]
                    # Start over so that the indexes are updated.
                    break
                if term == "/":
                    sub_terms[index - 1] = (
                        sub_terms[index - 1] / sub_terms[index + 1]
                    )
                    # Right-to-left deletion ensures no index errors.
                    del sub_terms[index + 1]
                    del sub_terms[index]
                    # Start over so that the indexes are updated.
                    break
    except ZeroDivisionError:
        return ZeroDivisionError
    return sub_terms


def evaluate_percents(sub_terms: list[Any]) -> list[Any]:
    """Convert any terms with percent signs to decimal form.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    """
    while "%" in sub_terms:
        for index, term in enumerate(sub_terms):
            if term == "%":
                sub_terms[index - 1] = sub_terms[index - 1] / 100
                del sub_terms[index]
                # Start over so that the indexes are updated.
                break
    return sub_terms


def evaluate_root_and_exp(sub_terms: list[Any]) -> Any:
    """Solve instances of exponentiation in the expression.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.
    exception : ValueError
        An exception showing that a complex number almost occurred.
        If there is no error, sub_terms is returned.

    """
    # Solve normally unless the expression reduces to the square root
    # of a negative number.
    try:
        while "√" in sub_terms or "²" in sub_terms:
            for index, term in enumerate(sub_terms):
                if term == "√":
                    if sub_terms[index + 1] < 0:
                        raise ValueError
                    sub_terms[index + 1] = sub_terms[index + 1] ** (1 / 2)
                    del sub_terms[index]
                    # Start over so that the indexes are updated.
                    break
                if term == "²":
                    sub_terms[index - 1] = sub_terms[index - 1] ** 2
                    del sub_terms[index]
                    # Start over so that the indexes are updated.
                    break
    except ValueError:
        return ValueError
    return sub_terms


def handle_expression() -> None:
    """Get an expression, check it, solve it, and display the answer.

    The expression is taken from the display. It is then parsed into
    terms and then the terms are formatted. The syntax of the
    expression is checked. The expression is then solved and the
    answer displayed. Instead of the answer, an error is displayed if
    one occurred during this process.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    global answer
    raw_expression: str = display.get("end - 1 lines", "end")
    expression: list[Any] = parse_expression(raw_expression)
    expression = add_missing_zeros(expression)

    if is_valid_syntax(expression):
        expression = convert_to_number(expression)
        answer = evaluate_expression(expression)
        if answer == ZeroDivisionError:
            display_error("divide by zero")
        elif answer == ValueError:
            display_error("square root of negative number")
        elif answer == SyntaxError:
            display_error("Malformed expression")
        else:
            display_answer(answer)
    elif not is_valid_syntax(expression):
        display_error("malformed expression")


def is_valid_syntax(expression: list[Any]) -> bool:
    """Check the syntax of a mathematical expression.

    Parameters
    ----------
    expression : list of str, int, or float
        The math expression separated into terms and operators.

    Returns
    -------
    bool
        True if valid syntax, false if invalid syntax.

    Notes
    -----
    For reference, listed below are all of the valid terms and
    operators, respectively:

    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ., ans
    /, *, -, +, ², √, (, ), %

    """
    num_left_paren: int = 0
    num_right_paren: int = 0
    last_index: int = len(expression) - 1
    last_paren: str = ""
    for index, term in enumerate(expression):
        # Numbers are allowed one decimal point.
        if term.count(".") > 1:
            return False
        # Certain operators cannot start or end an expression.
        if index == 0 and term in ["/", "*", "+", "²", "%", ")"]:
            return False
        if index == last_index and term in ["/", "*", "-", "+", "√", "("]:
            return False
        # A closing parenthesis cannot occur before an opening one.
        if num_left_paren + num_right_paren == 0 and term == ")":
            return False
        if term == "(":
            num_left_paren += 1
            last_paren = "("
        if term == ")":
            num_right_paren += 1
            last_paren = ")"
        # Certain operators are not allowed to be next to each other.
        if index != 0:
            if term in ["²", "%"] and expression[index - 1] in [
                "/", "*", "-", "+", "²", "√", "(",
            ]:
                return False
        # Certain operators are not allowed to be next to each other.
        if index != last_index:
            if term == "(" and expression[index + 1] in [
                "/", "*", "+", "²", ")", "%",
            ]:
                return False
            if (
                term in ["/", "*", "+", "²", "√", "%"]
                and expression[index+1] == term
            ):
                return False
            if term in ["/", "*"] and expression[index + 1] in ["/", "*", "+"]:
                return False
            if term in ["-", "+"] and expression[index + 1] in ["/", "*"]:
                return False
    if last_paren == "(" or num_left_paren != num_right_paren:
        return False
    return True


def launch_about_dialog(style: tk.ttk.Style, current_theme: str) -> None:
    """Launch a window describing the program.

    Parameters
    ----------
    style : tk.ttk.Style
        An object for styling widgets.
    current_theme : str
        Describes the current theme of the root window.

    Returns
    -------
    None

    """
    description = "A simple calculator built with Python and Tkinter."
    disclaimer = (
        "This program comes with absolutely no warranty.\n\n"
        "This program is licensed under the GNU \n"
        "General Public License, version 3 or later."
    )
    copyright = "Copyright © 2021 emerac"
    resources = (
        "For more information:\nhttps://www.gnu.org/licenses/gpl-3.0.txt"
    )
    dialog = tk.Toplevel(root)
    # Configure the widgets differently depending on the current theme.
    if current_theme == "Dark":
        dialog_frame = ttk.Frame(dialog, style="Precision.TFrame")
        description_label = ttk.Label(
            dialog_frame,
            text=description,
            style="LargeLabel.TLabel",
        )
        disclaimer_label = ttk.Label(
            dialog_frame,
            text=disclaimer,
            style="SmallLabel.TLabel",
        )
        copyright_label = ttk.Label(
            dialog_frame,
            text=copyright,
            style="SmallLabel.TLabel",
        )
        resources_label = ttk.Label(
            dialog_frame,
            text=resources,
            style="SmallLabel.TLabel",
        )
    elif current_theme == "Light":
        dialog_frame = ttk.Frame(dialog, style="LightPrecision.TFrame")
        description_label = ttk.Label(
            dialog_frame,
            text=description,
            style="LightLargeLabel.TLabel",
        )
        disclaimer_label = ttk.Label(
            dialog_frame,
            text=disclaimer,
            style="LightSmallLabel.TLabel",
        )
        copyright_label = ttk.Label(
            dialog_frame,
            text=copyright,
            style="LightSmallLabel.TLabel",
        )
        resources_label = ttk.Label(
            dialog_frame,
            text=resources,
            style="LightSmallLabel.TLabel",
        )

    dialog_frame.grid()
    description_label.grid()
    disclaimer_label.grid()
    copyright_label.grid()
    resources_label.grid()

    root_width: int = root.winfo_width()
    root_height: int = root.winfo_height()
    # Upper-left coordinates of the root window.
    root_x: int = root.winfo_rootx()
    root_y: int = root.winfo_rooty()
    # Without updating, the window dimensions are always 200x200.
    dialog.update()
    dialog_width: int = dialog.winfo_reqwidth()
    dialog_height: int = dialog.winfo_reqheight()
    # Upper-left coordinates of the dialog window.
    # This makes it always pop up in the middle of the root window.
    dialog_x = str(int(root_x + (root_width / 2) - (dialog_width / 2)))
    dialog_y = str(int(root_y + (root_height / 2) - (dialog_height / 2)))

    dialog.geometry("+{}+{}".format(dialog_x, dialog_y))
    dialog.title("Precision")
    dialog.resizable("false", "false")
    dialog.bind("<Escape>", lambda event: dialog.destroy())


def launch_precision_dialog(style: tk.ttk.Style, current_theme: str) -> None:
    """Launch a window asking user to choose the calculation precision.

    Parameters
    ----------
    style : tk.ttk.Style
        An object for styling widgets.
    current_theme : str
        Describes the current theme of the root window.

    Returns
    -------
    None

    """
    dialog = tk.Toplevel(root)
    # This variable holds the value in the spinbox.
    spinval = tk.StringVar()
    spinval.set(str(precision))
    # Configure the widgets differently depending on the current theme.
    if current_theme == "Dark":
        dialog_frame = ttk.Frame(dialog, style="Precision.TFrame")
        label = ttk.Label(
            dialog_frame,
            text="Number of decimals:",
            style="PrecisionLabel.TLabel",
        )
        spinbox = tk.Spinbox(
            dialog_frame,
            from_=0,
            to=100,
            textvariable=spinval,
            width=3,
            highlightbackground=dark_palette["active_button_bg"],
            readonlybackground=dark_palette["primary_bg"],
            foreground=dark_palette["primary_fg"],
            buttonbackground=dark_palette["button_bg"],
            state="readonly",
        )
        # Blank labels are a simple way to create padding.
        blank_label1 = ttk.Label(
            dialog_frame,
            style="PrecisionLabel.TLabel",
        )
        blank_label2 = ttk.Label(
            dialog_frame,
            style="PrecisionBlankLabel.TLabel",
        )
        blank_label3 = ttk.Label(
            dialog_frame,
            style="PrecisionBlankLabel.TLabel",
        )
        button = ttk.Button(
            dialog_frame,
            text="Ok",
            style="PrecisionOk.TButton",
            command=lambda: close_precision_dialog(dialog, spinval),
        )
    elif current_theme == "Light":
        dialog_frame = ttk.Frame(dialog, style="LightPrecision.TFrame")
        label = ttk.Label(
            dialog_frame,
            text="Number of decimals:",
            style="LightPrecisionLabel.TLabel",
        )
        spinbox = tk.Spinbox(
            dialog_frame,
            from_=0,
            to=100,
            textvariable=spinval,
            width=3,
            highlightbackground=light_palette["active_button_bg"],
            readonlybackground=light_palette["primary_bg"],
            foreground=light_palette["primary_fg"],
            buttonbackground=light_palette["button_bg"],
            state="readonly",
        )
        # Blank labels are simple way to create padding.
        blank_label1 = ttk.Label(
            dialog_frame,
            style="LightPrecisionLabel.TLabel",
        )
        blank_label2 = ttk.Label(
            dialog_frame,
            style="LightPrecisionBlankLabel.TLabel",
        )
        blank_label3 = ttk.Label(
            dialog_frame,
            style="LightPrecisionBlankLabel.TLabel",
        )
        button = ttk.Button(
            dialog_frame,
            text="Ok",
            style="LightPrecisionOk.TButton",
            command=lambda: close_precision_dialog(dialog, spinval),
        )

    dialog_frame.grid(column=0, row=0, sticky="nsew")
    label.grid(column=0, row=0)
    blank_label1.grid(column=1, row=0)
    spinbox.grid(column=2, row=0)
    blank_label2.grid(column=3, row=0)
    button.grid(column=4, row=0)
    blank_label3.grid(column=5, row=0)

    root_width: int = root.winfo_width()
    root_height: int = root.winfo_height()
    # Upper-left coordinates of the root window.
    root_x: int = root.winfo_rootx()
    root_y: int = root.winfo_rooty()
    # Without updating, the window dimensions are always 200x200.
    dialog.update()
    dialog_width: int = dialog.winfo_reqwidth()
    dialog_height: int = dialog.winfo_reqheight()
    # Upper-left coordinates of the dialog window.
    # This makes it always pop up in the middle of the root window.
    dialog_x = str(int(root_x + (root_width / 2) - (dialog_width / 2)))
    dialog_y = str(int(root_y + (root_height / 2) - (dialog_height / 2)))

    dialog.geometry("+{}+{}".format(dialog_x, dialog_y))
    dialog.title("Precision")
    dialog.resizable("false", "false")
    dialog.bind("<Escape>", lambda event: dialog.destroy())


def launch_shortcuts_dialog(style: tk.ttk.Style, current_theme: str) -> None:
    """Launch a window showing user available keyboard shortcuts.

    Parameters
    ----------
    style : tk.ttk.Style
        An object for styling widgets.
    current_theme : str
        Describes the current theme of the root window.

    Returns
    -------
    None

    """
    dialog = tk.Toplevel(root)
    # Configure the widgets differently depending on the current theme.
    if current_theme == "Dark":
        display = tk.Text(
            dialog,
            padx=8,
            pady=8,
            height=16,
            width=40,
            state="disabled",
            wrap="none",
            relief="flat",
            background=dark_palette["primary_bg"],
            foreground=dark_palette["primary_fg"],
            highlightbackground=dark_palette["active_button_bg"],
            font="TkTextFont",
        )
    elif current_theme == "Light":
        display = tk.Text(
            dialog,
            padx=8,
            pady=8,
            height=16,
            width=40,
            state="disabled",
            wrap="none",
            relief="flat",
            background=light_palette["primary_bg"],
            foreground=light_palette["primary_fg"],
            highlightbackground=light_palette["active_button_bg"],
            font="TkTextFont",
        )
    display.tag_configure("heading", font=("TkFixedFont", 14), underline=True)

    display.grid()

    display["state"] = "normal"
    display.insert("end", "General\n\n", ("heading"))
    display.insert("end", "Escape\t\tQuit the application\n")
    display.insert("end", "Ctrl+c\t\tClear entry history\n")
    display.insert("end", "Ctrl+k\t\tKeyboard shortcuts\n")
    display.insert("end", "\n\n")
    display.insert("end", "Keyboard entry\n\n", ("heading"))
    display.insert("end", "^\t\tSquare previous term\n")
    display.insert("end", "s\t\tSquare root\n")
    display.insert("end", "a\t\tInsert previous answer\n")
    display.insert("end", "c\t\tClear current line\n")
    display.insert("end", "Backspace\t\tUndo")
    display["state"] = "disabled"

    root_width: int = root.winfo_width()
    root_height: int = root.winfo_height()
    # Upper-left coordinates of the root window.
    root_x: int = root.winfo_rootx()
    root_y: int = root.winfo_rooty()
    # Without updating, the window dimensions are always 200x200.
    dialog.update()
    dialog_width: int = dialog.winfo_reqwidth()
    dialog_height: int = dialog.winfo_reqheight()
    # Upper-left coordinates of the dialog window.
    # This makes it always pop up in the middle of the root window.
    dialog_x = str(int(root_x + (root_width / 2) - (dialog_width / 2)))
    dialog_y = str(int(root_y + (root_height / 2) - (dialog_height / 2)))

    dialog.geometry("+{}+{}".format(dialog_x, dialog_y))
    dialog.title("Precision")
    dialog.resizable("false", "false")
    dialog.bind("<Escape>", lambda event: dialog.destroy())


def main() -> None:
    """Run the calculator application.

    When the module is imported, do not run the calculator application.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    root.mainloop()


def map_keypress_to_function(event: tk.Event, theme_toggle: str) -> None:
    """Invoke different functions based on the generated event.

    Parameters
    ----------
    event : tk.Event
        An event object generated by a user action.
    theme_toggle : str
        Describes the current theme of the root window.

    Returns
    -------
    None

    Notes
    -----
    When an event happens (buttonpress, keypress, etc.), Tk
    automatically generates an event. For example, when a key is
    pressed, Tk generates a KeyPress event detailing the key that was
    pressed, its symbolic name, and the character it represents.

    Before checking the details of the event, it is necessary to make
    sure it is the right kind of event. However, it appears there is
    no straightforward way of doing so. The attributes of the event
    can be shown using `dir()`. Only KeyPress events will have a
    `keysym` attribute.

    The calculator buttons look like they were pressed when they are
    directly clicked, but this doesn't happen when their corresponding
    key is pressed. To do this, the program follows these steps:

        1. Check if event is a KeyPress event.
        2. Check if the keypress has a mapping to a button object.
        3. Retrieve the button's name.
        4. Retrieve the button object via the nametowidget method.
        5. Change the styling of the button object for short time.

    Since the clear and equal button have their own separate colors,
    they need their own separate handlers.

    """
    valid_characters = [
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        ".",
        "%",
        "/",
        "*",
        "-",
        "+",
        "=",
        "(",
        ")",
        "^",
    ]
    valid_modifiers = ["Shift_L", "Shift_R"]
    key_to_button_map = {
        # Keypad characters.
        "KP_0": "b0",
        "KP_1": "b1",
        "KP_2": "b2",
        "KP_3": "b3",
        "KP_4": "b4",
        "KP_5": "b5",
        "KP_6": "b6",
        "KP_7": "b7",
        "KP_8": "b8",
        "KP_9": "b9",
        "KP_Divide": "bdiv",
        "KP_Multiply": "bmult",
        "KP_Subtract": "bsub",
        "KP_Add": "badd",
        "KP_Decimal": "bdec",
        "KP_Enter": "bequal",
        # Normal keyboard chars that correspond to keypad chars.
        "0": "b0",
        "1": "b1",
        "2": "b2",
        "3": "b3",
        "4": "b4",
        "5": "b5",
        "6": "b6",
        "7": "b7",
        "8": "b8",
        "9": "b9",
        "slash": "bdiv",
        "asterisk": "bmult",
        "minus": "bsub",
        "plus": "badd",
        "period": "bdec",
        "Return": "bequal",
        # Special characters.
        "equal": "bequal",
        "percent": "bper",
        "parenleft": "bleft_paren",
        "parenright": "bright_paren",
        "asciicircum": "bexp",
        "s": "broot",
        "S": "broot",
        "a": "bans",
        "A": "bans",
        "c": "bclear",
        "C": "bclear",
        "BackSpace": "bundo",
    }
    # Make buttons briefly appear sunken when a keypress event occurs.
    if theme_toggle == "Dark":
        # The dark theme clear button
        if (
            "keysym" in dir(event)
            and event.keysym in key_to_button_map
            and (event.keysym == "c" or event.keysym == "C")
        ):
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="ClearPressed.TButton")
            mainframe.after(70, lambda: b.config(style="Clear.TButton"))
        # The dark theme equal button
        elif (
            "keysym" in dir(event)
            and event.keysym in key_to_button_map
            and (
                event.keysym == "KP_Enter"
                or event.keysym == "equal"
                or event.keysym == "Return"
            )
        ):
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="EqualPressed.TButton")
            mainframe.after(70, lambda: b.config(style="Equal.TButton"))
        # The dark theme regular button
        elif "keysym" in dir(event) and event.keysym in key_to_button_map:
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="InputPressed.TButton")
            mainframe.after(70, lambda: b.config(style="Input.TButton"))
    # Make buttons briefly appear sunken when a keypress event occurs.
    elif theme_toggle == "Light":
        # The light theme clear button
        if (
            "keysym" in dir(event)
            and event.keysym in key_to_button_map
            and (event.keysym == "c" or event.keysym == "C")
        ):
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="LightClearPressed.TButton")
            mainframe.after(70, lambda: b.config(style="LightClear.TButton"))
        # The light theme equal button
        elif (
            "keysym" in dir(event)
            and event.keysym in key_to_button_map
            and (
                event.keysym == "KP_Enter"
                or event.keysym == "equal"
                or event.keysym == "Return"
            )
        ):
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="LightEqualPressed.TButton")
            mainframe.after(70, lambda: b.config(style="LightEqual.TButton"))
        # The light theme regular button
        elif "keysym" in dir(event) and event.keysym in key_to_button_map:
            b_name = key_to_button_map[event.keysym]
            b = mainframe.nametowidget(b_name)
            b.config(style="LightInputPressed.TButton")
            mainframe.after(70, lambda: b.config(style="LightInput.TButton"))

    # Invoke functions based on the key that was pressed.
    if event.keysym == "Escape":
        quit_program()
    elif event.keysym == "BackSpace":
        delete_char()
    elif event.char.lower() == "c":
        clear_expression()
    elif event.char.lower() == "a":
        write_to_display(event.char)
    elif event.char.lower() == "s":
        write_to_display(event.char)
    elif (
        event.keysym == "Return"
        or event.keysym == "KP_Enter"
        or event.char == "="
    ):
        handle_expression()
    elif event.char in valid_characters:
        write_to_display(event.char)
    elif event.keysym in valid_modifiers:
        pass


def parse_expression(expression: str) -> list[Any]:
    """Separate a string representing an expression into terms.

    Remove characters that are irrelevant to the math expression and
    separate the remaining string into mathematical terms (i.e.
    numbers and operators).

    Parameters
    ----------
    expression : str
        A full line of text taken from the display.

    Returns
    -------
    terms : list of str, int, float
        The cleaned expression separated into terms.

    Notes
    -----
    Certain elements are not parsed in this function. For example,
    this function does not differentiate between a minus sign and a
    negative sign. A separate function must perform that action.

    """
    global answer
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
    operators = ["/", "*", "-", "+", "²", "√", "(", ")", "%"]
    prompt = ["⮞", " "]
    terms = []
    temp_term = []

    # Let the ans keyword be treated as any other number.
    if "ans" in expression:
        expression = expression.replace("ans", str(answer))
    exp_list = list(expression)
    # Remove the prompt characters.
    for index in range(len(exp_list) - 1, -1, -1):
        if exp_list[index] in prompt:
            del exp_list[index]
    # Tk always puts a newline char at the end of each line.
    exp_list.pop()
    exp_last_index = len(exp_list) - 1

    # Loop through the expression, breaking it into terms.
    for index, char in enumerate(exp_list):
        # Since the length of the number is unknown, start building a
        # mini-term. When it is completed, turn it into a full term.
        if (
            index != exp_last_index
            and char in numbers
            and exp_list[index + 1] in numbers
        ):
            temp_term.append(char)
        # If next item is not a num, then current num is complete.
        elif (
            index != exp_last_index
            and char in numbers
            and not exp_list[index + 1] in numbers
        ):
            temp_term.append(char)
            term = "".join(temp_term)
            terms.append(term)
            temp_term = []
        elif char in operators:
            terms.append(char)
        # Handle numbers at the end without causing an index error.
        elif (
            index == exp_last_index
            and char in numbers
        ):
            temp_term.append(char)
            term = "".join(temp_term)
            terms.append(term)
            temp_term = []
        else:
            # This exception should never be raised.
            raise NotImplementedError(
                "Parser was unable to handle the expression."
            )
    if len(terms) == 0:
        terms = ["0"]
    return terms


def parse_minuses(sub_terms: list[Any]) -> list[Any]:
    """Change minus signs into negative signs if applicable.

    Parameters
    ----------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    Returns
    -------
    sub_terms : list of str, int, or float
        A math expression with no parentheses, separated into terms.

    """
    # These operators are allowed to the left of a minus sign.
    left_operators = ["²", "%"]
    # These operators are allowed to the right of a minus sign.
    right_operators = ["/", "*", "-", "+", "√"]
    # A minus sign at the beginning of an expression can be thought of
    # as subtracting the expression from zero.
    if len(sub_terms) >= 2:
        if sub_terms[0] == "-":
            sub_terms.insert(0, 0)
    # Looping backwards, otherwise deletion may cause index errors.
    for index in range(len(sub_terms) - 1, -1, -1):
        if index != 0:
            # Convert a minus symbol next to an operator and a number
            # to a negation of that number (e.g. the second 3 in
            # -3/-3 would become a negative 3).
            if (
                sub_terms[index] == "-"
                and sub_terms[index - 1] in right_operators
            ):
                right_term_type = type(sub_terms[index + 1])
                if right_term_type == float:
                    sub_terms[index] = float("-" + str(sub_terms[index + 1]))
                elif right_term_type == int:
                    sub_terms[index] = int("-" + str(sub_terms[index + 1]))
                del sub_terms[index + 1]
            if (
                sub_terms[index] == "-"
                and sub_terms[index - 1] in left_operators
            ):
                continue
    return sub_terms


def set_dark_theme() -> None:
    """Reconfigure all widgets to a dark theme.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    root["background"] = dark_palette["primary_bg"]
    mainframe.config(style="Mainframe.TFrame")
    menubar.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    win_menu.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    theme_menu.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    font_menu.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    display_menu.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    help_menu.config(
        background=dark_palette["primary_bg"],
        activebackground=dark_palette["active_button_bg"],
        foreground=dark_palette["primary_fg"],
        activeforeground=dark_palette["primary_fg"],
    )
    display.config(
        background=dark_palette["primary_bg"],
        foreground=dark_palette["primary_fg"],
    )
    b0.config(style="Input.TButton")
    b1.config(style="Input.TButton")
    b2.config(style="Input.TButton")
    b3.config(style="Input.TButton")
    b4.config(style="Input.TButton")
    b5.config(style="Input.TButton")
    b6.config(style="Input.TButton")
    b7.config(style="Input.TButton")
    b8.config(style="Input.TButton")
    b9.config(style="Input.TButton")
    bdec.config(style="Input.TButton")
    bper.config(style="Input.TButton")
    bleft_paren.config(style="Input.TButton")
    bright_paren.config(style="Input.TButton")
    broot.config(style="Input.TButton")
    bexp.config(style="Input.TButton")
    bdiv.config(style="Input.TButton")
    bmult.config(style="Input.TButton")
    bsub.config(style="Input.TButton")
    badd.config(style="Input.TButton")
    bans.config(style="Input.TButton")
    bundo.config(style="Input.TButton")
    bclear.config(style="Clear.TButton")
    bequal.config(style="Equal.TButton")


def set_fontsize(font_toggle: str) -> None:
    """Change the fontsize of the display.

    Parameters
    ----------
    font_toggle : str
        The user's choice of font size.

    Returns
    -------
    None

    Notes
    -----
    When the font size is changed, the size of the entire display is
    also changed. In most other places where height shows up, Tk uses
    pixels. However, in this instance, Tk uses the number of lines
    that can be displayed using the current fontsize.

    If the dimensions of the root window are not specified, the root
    window will change size as well.

    """
    if font_toggle == "Small":
        display.config(font=("TkFixedFont", 8))
    elif font_toggle == "Medium":
        display.config(font=("TkFixedFont", 12))
    elif font_toggle == "Large":
        display.config(font=("TkFixedFont", 20))


def set_light_theme() -> None:
    """Reconfigure all widgets to a light theme.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    root["background"] = light_palette["primary_bg"]
    mainframe.config(style="LightMainframe.TFrame")
    menubar.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    win_menu.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    theme_menu.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    font_menu.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    display_menu.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    help_menu.config(
        background=light_palette["primary_bg"],
        activebackground=light_palette["active_button_bg"],
        foreground=light_palette["primary_fg"],
        activeforeground=light_palette["primary_fg"],
    )
    display.config(
        background=light_palette["primary_bg"],
        foreground=light_palette["primary_fg"],
    )
    b0.config(style="LightInput.TButton")
    b1.config(style="LightInput.TButton")
    b2.config(style="LightInput.TButton")
    b3.config(style="LightInput.TButton")
    b4.config(style="LightInput.TButton")
    b5.config(style="LightInput.TButton")
    b6.config(style="LightInput.TButton")
    b7.config(style="LightInput.TButton")
    b8.config(style="LightInput.TButton")
    b9.config(style="LightInput.TButton")
    bdec.config(style="LightInput.TButton")
    bper.config(style="LightInput.TButton")
    bleft_paren.config(style="LightInput.TButton")
    bright_paren.config(style="LightInput.TButton")
    broot.config(style="LightInput.TButton")
    bexp.config(style="LightInput.TButton")
    bdiv.config(style="LightInput.TButton")
    bmult.config(style="LightInput.TButton")
    bsub.config(style="LightInput.TButton")
    badd.config(style="LightInput.TButton")
    bans.config(style="LightInput.TButton")
    bundo.config(style="LightInput.TButton")
    bclear.config(style="LightClear.TButton")
    bequal.config(style="LightEqual.TButton")


def quit_program(event: Optional[tk.Event] = None) -> None:
    """Exit the program safely.

    Parameters
    ----------
    None

    Returns
    -------
    None

    """
    root.quit()


def write_to_display(term: str, event=None) -> None:
    """Write a term to the display.

    Parameters
    ----------
    term : str
        The term to be displayed.

    Returns
    -------
    None

    """
    display["state"] = "normal"
    # Interpret some terms differently so they display nicely.
    if term == "x²":
        display.insert("end", "²")
    elif term == "^":
        display.insert("end", "²")
    elif term == "a":
        display.insert("end", "ans")
    elif term == "s":
        display.insert("end", "√")
    else:
        display.insert("end", term)
    display["state"] = "disabled"
    display.see("end")
    mainframe.focus_set()


# The answer to the most recent calculation.
answer: Union[int, float] = 0
# The number of trailing digits when displaying answer.
precision: int = 2
# The padding the surrounds each button.
bpad: int = 3

# Application colors when the dark theme is chosen.
dark_palette = {
    "primary_fg": "#F2F2F2",
    "primary_bg": "#262626",
    "button_bg": "#333333",
    "clear_bg": "#581C0E",
    "equal_bg": "#0E1E58",
    "active_button_bg": "#404040",
    "active_clear_bg": "#6E2211",
    "active_equal_bg": "#11256E",
}
# Application colors when the light theme is chosen.
light_palette = {
    "primary_fg": "#262626",
    "primary_bg": "#F2F2F2",
    "button_bg": "#E6E6E6",
    "clear_bg": "#E77C65",
    "equal_bg": "#6581E7",
    "active_button_bg": "#D9D9D9",
    "active_clear_bg": "#E36B4F",
    "active_equal_bg": "#4F6FE3"
}
# Menu radiobuttons cannot change color, so they should have a color
# that is visible in any theme.
medium_palette = {"radio_select": "#8C8C8C"}

# The primary application window.
root = tk.Tk()
root.title("Calculator")
root.geometry("540x408+100+100")
# A minsize keeps the the buttons and the display readable.
root.minsize(250, 400)
# Root's background and mainframe background should be the same color
# otherwise it will appear as if there is a border around the window
# where root and mainframe meet. This is due to mainframe's padding.
root["background"] = dark_palette["primary_bg"]
root.option_add("*tearOff", "false")

# All themed widgets receive their styles from this style object.
style = ttk.Style()
# Dark themes.
style.configure(
    "Mainframe.TFrame",
    background=dark_palette["primary_bg"],
)
style.configure(
    "Precision.TFrame",
    background=dark_palette["primary_bg"],
)
style.configure(
    "Input.TButton",
    background=dark_palette["button_bg"],
    foreground=dark_palette["primary_fg"],
)
style.configure(
    "InputPressed.TButton",
    background=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    relief="sunken",
)
style.configure(
    "Clear.TButton",
    background=dark_palette["clear_bg"],
    foreground=dark_palette["primary_fg"]
)
style.configure(
    "ClearPressed.TButton",
    background=dark_palette["active_clear_bg"],
    foreground=dark_palette["primary_fg"],
    relief="sunken"
)
style.configure(
    "Equal.TButton",
    background=dark_palette["equal_bg"],
    foreground=dark_palette["primary_fg"],
)
style.configure(
    "EqualPressed.TButton",
    background=dark_palette["active_equal_bg"],
    foreground=dark_palette["primary_fg"],
    relief="sunken"
)
style.configure(
    "PrecisionLabel.TLabel",
    padding=10,
    sticky="nsew",
    background=dark_palette["primary_bg"],
    foreground=dark_palette["primary_fg"],
)
style.configure(
    "PrecisionBlankLabel.TLabel",
    padding=10,
    sticky="nsew",
    background=dark_palette["primary_bg"],
    foreground=dark_palette["primary_fg"],
)
style.configure(
    "PrecisionOk.TButton",
    background=dark_palette["button_bg"],
    foreground=dark_palette["primary_fg"],
    width=3,
)
style.configure(
    "LargeLabel.TLabel",
    font=("TkDefaultFont", 12),
    padding=10,
    sticky="nsew",
    justify="center",
    background=dark_palette["primary_bg"],
    foreground=dark_palette["primary_fg"],
)
style.configure(
    "SmallLabel.TLabel",
    font=("TkDefaultFont", 8),
    padding=10,
    sticky="nsew",
    justify="center",
    background=dark_palette["primary_bg"],
    foreground=dark_palette["primary_fg"],
)
# Light themes.
style.configure(
    "LightMainframe.TFrame",
    background=light_palette["primary_bg"],
)
style.configure(
    "LightPrecision.TFrame",
    background=light_palette["primary_bg"],
)
style.configure(
    "LightInput.TButton",
    background=light_palette["button_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightInputPressed.TButton",
    background=light_palette["active_button_bg"],
    foreground=light_palette["primary_fg"],
    relief="sunken",
)
style.configure(
    "LightClear.TButton",
    background=light_palette["clear_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightClearPressed.TButton",
    background=light_palette["active_clear_bg"],
    foreground=light_palette["primary_fg"],
    relief="sunken",
)
style.configure(
    "LightEqual.TButton",
    background=light_palette["equal_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightEqualPressed.TButton",
    background=light_palette["active_equal_bg"],
    foreground=light_palette["primary_fg"],
    relief="sunken",
)
style.configure(
    "LightPrecisionLabel.TLabel",
    padding=10,
    sticky="nsew",
    background=light_palette["primary_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightPrecisionBlankLabel.TLabel",
    padding=10,
    sticky="nsew",
    background=light_palette["primary_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightPrecisionOk.TButton",
    background=light_palette["button_bg"],
    foreground=light_palette["primary_fg"],
    width=3,
)
style.configure(
    "LightLargeLabel.TLabel",
    font=("TkDefaultFont", 12),
    padding=10,
    sticky="nsew",
    justify="center",
    background=light_palette["primary_bg"],
    foreground=light_palette["primary_fg"],
)
style.configure(
    "LightSmallLabel.TLabel",
    font=("TkDefaultFont", 8),
    padding=10,
    sticky="nsew",
    justify="center",
    background=light_palette["primary_bg"],
    foreground=light_palette["primary_fg"],
)
# When hovering over button, the button's background should be lighter.
# Dark themes.
style.map(
    "Input.TButton",
    background=[("active", dark_palette["active_button_bg"])],
)
style.map(
    "Clear.TButton",
    background=[("active", dark_palette["active_clear_bg"])],
)
style.map(
    "Equal.TButton",
    background=[("active", dark_palette["active_equal_bg"])],
)
style.map(
    "PrecisionOk.TButton",
    background=[("active", dark_palette["active_button_bg"])],
)
# Light themes.
style.map(
    "LightInput.TButton",
    background=[("active", light_palette["active_button_bg"])],
)
style.map(
    "LightClear.TButton",
    background=[("active", light_palette["active_clear_bg"])],
)
style.map(
    "LightEqual.TButton",
    background=[("active", light_palette["active_equal_bg"])],
)
style.map(
    "LightPrecisionOk.TButton",
    background=[("active", light_palette["active_button_bg"])],
)

# Mainframe holds all of the application's content.
mainframe = ttk.Frame(root, style="Mainframe.TFrame")
# The text widget is a classic Tk widget, not a themed Tk widget.
# Height corresponds to the number of lines possible at the fontsize.
# Width corresponds to the number of characters at the fontsize.
display = tk.Text(
    mainframe,
    padx=8,
    pady=8,
    width=10,
    height=10,
    wrap="none",
    relief="flat",
    state="disabled",
    background=dark_palette["primary_bg"],
    foreground=dark_palette["primary_fg"],
    highlightbackground=dark_palette["active_button_bg"],
    font=("TkFixedFont", 12),
)
menubar = tk.Menu(
    root,
    relief="flat",
    borderwidth=0,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
root["menu"] = menubar

# The window menu is a menu within the menubar menu.
win_menu = tk.Menu(
    menubar,
    relief="flat",
    borderwidth=3,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
menubar.add_cascade(
    label="Window",
    menu=win_menu
)
win_menu.add_command(
    label="Default",
    command=lambda: change_window_geometry("Default"),
)
win_menu.add_command(
    label="Maximize",
    command=lambda: change_window_geometry("Maximize"),
)
win_menu.add_separator()
win_menu.add_command(
    label="Snap Left",
    command=lambda: change_window_geometry("Snap Left"),
)
win_menu.add_command(
    label="Snap Right",
    command=lambda: change_window_geometry("Snap Right"),
)
win_menu.add_separator()
win_menu.add_command(
    label="Quit",
    command=quit_program, accelerator="Escape",
)

# Using a variable, the radiobutton can be preselected.
theme_toggle = tk.StringVar()
theme_toggle.set("Dark")
# The theme menu is a menu within the menubar menu.
theme_menu = tk.Menu(
    menubar,
    relief="flat",
    borderwidth=3,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
menubar.add_cascade(
    label="Theme",
    menu=theme_menu,
)
theme_menu.add_radiobutton(
    label="Light",
    command=set_light_theme,
    selectcolor=medium_palette["radio_select"],
    variable=theme_toggle,
)
theme_menu.add_radiobutton(
    label="Dark",
    command=set_dark_theme,
    selectcolor=medium_palette["radio_select"],
    variable=theme_toggle,
)

# Using a variable, the radiobutton can be preselected.
font_toggle = tk.StringVar()
font_toggle.set("Medium")
# The font menu is a menu within the menubar menu.
font_menu = tk.Menu(
    menubar,
    relief="flat",
    borderwidth=3,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
menubar.add_cascade(
    label="Font",
    menu=font_menu,
)
font_menu.add_radiobutton(
    label="Small",
    command=lambda: set_fontsize(font_toggle.get()),
    selectcolor=medium_palette["radio_select"],
    variable=font_toggle,
)
font_menu.add_radiobutton(
    label="Medium",
    command=lambda: set_fontsize(font_toggle.get()),
    selectcolor=medium_palette["radio_select"],
    variable=font_toggle,
)
font_menu.add_radiobutton(
    label="Large",
    command=lambda: set_fontsize(font_toggle.get()),
    selectcolor=medium_palette["radio_select"],
    variable=font_toggle,
)

# The display menu is a menu within the menubar menu.
display_menu = tk.Menu(
    menubar,
    relief="flat",
    borderwidth=3,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
menubar.add_cascade(
    label="Display",
    menu=display_menu,
)
display_menu.add_command(
    label="Clear Display",
    command=clear_entire_display,
    accelerator="Ctrl-C",
)
display_menu.add_separator()
display_menu.add_command(
    label="Precision...",
    command=lambda: launch_precision_dialog(style, theme_toggle.get()),
)

# The help menu is a menu within the menubar menu.
help_menu = tk.Menu(
    menubar,
    relief="flat",
    borderwidth=3,
    activeborderwidth=0,
    background=dark_palette["primary_bg"],
    activebackground=dark_palette["active_button_bg"],
    foreground=dark_palette["primary_fg"],
    activeforeground=dark_palette["primary_fg"],
)
menubar.add_cascade(
    label="Help",
    menu=help_menu,
)
help_menu.add_command(
    label="Keyboard Shortcuts",
    accelerator="Ctrl-K",
    command=lambda: launch_shortcuts_dialog(style, theme_toggle.get()),
)
help_menu.add_separator()
help_menu.add_command(
    label="About",
    command=lambda: launch_about_dialog(style, theme_toggle.get()),
)

# Create button objects and give them commands to invoke when pressed.
b0: ttk.Button = ttk.Button(
    mainframe,
    text="0",
    name="b0",
    style="Input.TButton",
    command=lambda: write_to_display(b0["text"]),
)
b1: ttk.Button = ttk.Button(
    mainframe,
    text="1",
    name="b1",
    style="Input.TButton",
    command=lambda: write_to_display(b1["text"]),
)
b2: ttk.Button = ttk.Button(
    mainframe,
    text="2",
    name="b2",
    style="Input.TButton",
    command=lambda: write_to_display(b2["text"]),
)
b3: ttk.Button = ttk.Button(
    mainframe,
    text="3",
    name="b3",
    style="Input.TButton",
    command=lambda: write_to_display(b3["text"]),
)
b4: ttk.Button = ttk.Button(
    mainframe,
    text="4",
    name="b4",
    style="Input.TButton",
    command=lambda: write_to_display(b4["text"]),
)
b5: ttk.Button = ttk.Button(
    mainframe,
    text="5",
    name="b5",
    style="Input.TButton",
    command=lambda: write_to_display(b5["text"]),
)
b6: ttk.Button = ttk.Button(
    mainframe,
    text="6",
    name="b6",
    style="Input.TButton",
    command=lambda: write_to_display(b6["text"]),
)
b7: ttk.Button = ttk.Button(
    mainframe,
    text="7",
    name="b7",
    style="Input.TButton",
    command=lambda: write_to_display(b7["text"]),
)
b8: ttk.Button = ttk.Button(
    mainframe,
    text="8",
    name="b8",
    style="Input.TButton",
    command=lambda: write_to_display(b8["text"]),
)
b9: ttk.Button = ttk.Button(
    mainframe,
    text="9",
    name="b9",
    style="Input.TButton",
    command=lambda: write_to_display(b9["text"]),
)
bdec: ttk.Button = ttk.Button(
    mainframe,
    text=".",
    name="bdec",
    style="Input.TButton",
    command=lambda: write_to_display(bdec["text"]),
)
bper: ttk.Button = ttk.Button(
    mainframe,
    text="%",
    name="bper",
    style="Input.TButton",
    command=lambda: write_to_display(bper["text"]),
)
bleft_paren: ttk.Button = ttk.Button(
    mainframe,
    text="(",
    name="bleft_paren",
    style="Input.TButton",
    command=lambda: write_to_display(bleft_paren["text"]),
)
bright_paren: ttk.Button = ttk.Button(
    mainframe,
    text=")",
    name="bright_paren",
    style="Input.TButton",
    command=lambda: write_to_display(bright_paren["text"]),
)
broot: ttk.Button = ttk.Button(
    mainframe,
    text="√",
    name="broot",
    style="Input.TButton",
    command=lambda: write_to_display(broot["text"]),
)
bexp: ttk.Button = ttk.Button(
    mainframe,
    text="x²",
    name="bexp",
    style="Input.TButton",
    command=lambda: write_to_display(bexp["text"]),
)
bdiv: ttk.Button = ttk.Button(
    mainframe,
    text="/",
    name="bdiv",
    style="Input.TButton",
    command=lambda: write_to_display(bdiv["text"]),
)
bmult: ttk.Button = ttk.Button(
    mainframe,
    text="*",
    name="bmult",
    style="Input.TButton",
    command=lambda: write_to_display(bmult["text"]),
)
bsub: ttk.Button = ttk.Button(
    mainframe,
    text="−",
    name="bsub",
    style="Input.TButton",
    command=lambda: write_to_display("-"),
)
badd: ttk.Button = ttk.Button(
    mainframe,
    text="+",
    name="badd",
    style="Input.TButton",
    command=lambda: write_to_display(badd["text"]),
)
bans: ttk.Button = ttk.Button(
    mainframe,
    text="ans",
    name="bans",
    style="Input.TButton",
    command=lambda: write_to_display(bans["text"]),
)
bundo: ttk.Button = ttk.Button(
    mainframe,
    text="↶",
    name="bundo",
    style="Input.TButton",
    command=lambda: delete_char(),
)
bclear: ttk.Button = ttk.Button(
    mainframe,
    text="C",
    name="bclear",
    style="Clear.TButton",
    command=lambda: clear_expression(),
)
bequal: ttk.Button = ttk.Button(
    mainframe,
    text="=",
    name="bequal",
    style="Equal.TButton",
    command=lambda: handle_expression(),
)

# Force widgets to fill their given space.
mainframe.grid(column=0, row=0, sticky="nsew", padx=3, pady=3)
# Display should take up the entire first row row.
display.grid(column=0, row=0, columnspan=6, sticky="nsew", padx=3, pady=3)
# Grid all of the buttons.
b0.grid(column=0, row=4, sticky="nsew", padx=bpad, pady=bpad)
b1.grid(column=0, row=3, sticky="nsew", padx=bpad, pady=bpad)
b2.grid(column=1, row=3, sticky="nsew", padx=bpad, pady=bpad)
b3.grid(column=2, row=3, sticky="nsew", padx=bpad, pady=bpad)
b4.grid(column=0, row=2, sticky="nsew", padx=bpad, pady=bpad)
b5.grid(column=1, row=2, sticky="nsew", padx=bpad, pady=bpad)
b6.grid(column=2, row=2, sticky="nsew", padx=bpad, pady=bpad)
b7.grid(column=0, row=1, sticky="nsew", padx=bpad, pady=bpad)
b8.grid(column=1, row=1, sticky="nsew", padx=bpad, pady=bpad)
b9.grid(column=2, row=1, sticky="nsew", padx=bpad, pady=bpad)
bdec.grid(column=1, row=4, sticky="nsew", padx=bpad, pady=bpad)
bper.grid(column=2, row=4, sticky="nsew", padx=bpad, pady=bpad)
bleft_paren.grid(column=4, row=2, sticky="nsew", padx=bpad, pady=bpad)
bright_paren.grid(column=5, row=2, sticky="nsew", padx=bpad, pady=bpad)
broot.grid(column=5, row=3, sticky="nsew", padx=bpad, pady=bpad)
bexp.grid(column=4, row=3, sticky="nsew", padx=bpad, pady=bpad)
bdiv.grid(column=3, row=1, sticky="nsew", padx=bpad, pady=bpad)
bmult.grid(column=3, row=2, sticky="nsew", padx=bpad, pady=bpad)
bsub.grid(column=3, row=3, sticky="nsew", padx=bpad, pady=bpad)
badd.grid(column=3, row=4, sticky="nsew", padx=bpad, pady=bpad)
bans.grid(column=4, row=4, sticky="nsew", padx=bpad, pady=bpad)
bundo.grid(column=4, row=1, sticky="nsew", padx=bpad, pady=bpad)
bclear.grid(column=5, row=1, sticky="nsew", padx=bpad, pady=bpad)
bequal.grid(column=5, row=4, sticky="nsew", padx=bpad, pady=bpad)

# Set the behavior of the widgets when the window is resized.
# Root should at all times only have one row and one column.
root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)
frame_columns = mainframe.grid_size()[0]
frame_rows = mainframe.grid_size()[1]

# The display should basically be the only thing that resizes.
mainframe.rowconfigure(0, weight=80)
for row in range(1, frame_rows):
    mainframe.rowconfigure(row, weight=1)
for column in range(0, frame_columns):
    mainframe.columnconfigure(column, weight=1)

display_prompt()
# Bindings only work if the frame has focus and by default it doesn't.
mainframe.focus_set()
mainframe.bind(
    "<Control-c>", lambda event: clear_entire_display(),
)
mainframe.bind(
    "<Control-k>",
    lambda event: launch_shortcuts_dialog(style, theme_toggle.get()),
)
mainframe.bind(
    "<KeyPress>",
    lambda event: map_keypress_to_function(event, theme_toggle.get()),
)

if __name__ == "__main__":
    main()
