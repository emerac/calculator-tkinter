#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A test module for tkcalc.py.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import sys
import unittest

# Allow imports to be referenced from one directory level up.
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), "..")),
)

from tkcalc import tkcalc


class TestTkcalc(unittest.TestCase):
    def test_add_missing_zeros(self):
        self.assertEqual(
            tkcalc.add_missing_zeros(["0.123"]),
            ["0.123"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros([".123"]),
            ["0.123"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["123"]),
            ["123"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["123.0"]),
            ["123.0"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["123."]),
            ["123.0"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["0.1", "0.2", "0.3"]),
            ["0.1", "0.2", "0.3"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros([".1", ".2", ".3"]),
            ["0.1", "0.2", "0.3"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["1", "2", "3"]),
            ["1", "2", "3"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["1.0", "2.0", "3.0"]),
            ["1.0", "2.0", "3.0"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["1.", "2.", "3."]),
            ["1.0", "2.0", "3.0"],
        )
        self.assertEqual(
            tkcalc.add_missing_zeros(["-", "2.", "3."]),
            ["-", "2.0", "3.0"],
        )

    def test_check_minuses(self):
        self.assertEqual(
            tkcalc.check_minuses(["-", 1]),
            ["-", 1],
        )
        self.assertEqual(
            tkcalc.check_minuses(["-"]),
            ["-"],
        )
        self.assertEqual(
            tkcalc.check_minuses(["-", "/", 1]),
            SyntaxError,
        )
        self.assertEqual(
            tkcalc.check_minuses([1, "-", "*"]),
            SyntaxError,
        )

    def test_convert_to_number(self):
        self.assertEqual(
            tkcalc.convert_to_number(["1"]),
            [1],
        )
        self.assertEqual(
            tkcalc.convert_to_number(["1.0"]),
            [1.0],
        )
        self.assertEqual(
            tkcalc.convert_to_number(["-"]),
            ["-"],
        )
        self.assertEqual(
            tkcalc.convert_to_number(["-", "1"]),
            ["-", 1],
        )
        self.assertEqual(
            tkcalc.convert_to_number(["-", "1.0"]),
            ["-", 1.0],
        )

    def test_evaluate_add_and_sub(self):
        self.assertEqual(
            len(tkcalc.evaluate_add_and_sub([1, "+", 1, "-", -1])),
            1,
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([1, "+", 0]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([1.1, "+", 0]),
            [1.1],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([1.1, "+", -1.1]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([-1.1, "+", -1.1]),
            [-2.2],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([1, "-", 1]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([1, "-", -1]),
            [2],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([-1, "-", -1]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([-1, "-", -1, "+", 1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([-1, "-", -1, "+", 1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_add_and_sub([-1, "-", -1, "+", -1]),
            [-1],
        )
        self.assertIsInstance(
            tkcalc.evaluate_add_and_sub([1, "+", 1]),
            list,
        )
        self.assertIsInstance(
            tkcalc.evaluate_add_and_sub([1, "+", 1])[0],
            int,
        )
        self.assertIsInstance(
            tkcalc.evaluate_add_and_sub([1.0, "+", 1])[0],
            float,
        )

        self.assertNotIsInstance(
            tkcalc.evaluate_add_and_sub([1, "+", 1])[0],
            str,
        )
        self.assertIn(
            "/",
            tkcalc.evaluate_add_and_sub([1, "/", 1]),
        )
        self.assertIn(
            "*",
            tkcalc.evaluate_add_and_sub([1, "*", 1]),
        )
        self.assertIn(
            "²",
            tkcalc.evaluate_add_and_sub([1, "²", 1]),
        )
        self.assertIn(
            "√",
            tkcalc.evaluate_add_and_sub([1, "√", 1]),
        )
        self.assertNotIn(
            "+",
            tkcalc.evaluate_add_and_sub([1, "+", 1, "-", -1]),
        )
        self.assertNotIn(
            "-",
            tkcalc.evaluate_add_and_sub([1, "+", 1, "-", -1]),
        )

    def test_evaluate_expression(self):
        self.assertEqual(
            tkcalc.evaluate_expression([1, "-", "+", 1]),
            SyntaxError,
        )
        self.assertEqual(
            tkcalc.evaluate_expression(["√", -1]),
            ValueError,
        )
        self.assertEqual(
            tkcalc.evaluate_expression([1, "/", 0]),
            ZeroDivisionError,
        )
        self.assertEqual(
            tkcalc.evaluate_expression([3, "*", "(", "√", 4, "/", 200, "%", ")"]),
            3.0,
        )
        self.assertEqual(
            tkcalc.evaluate_expression(["-", 3, "/", "(", "√", 4, "²", "/", 200, "%", ")"]),
            -1.5,
        )
        self.assertEqual(
            tkcalc.evaluate_expression(
                ["-", "(", "√", 9, "²", "/", "-", 300, "%", ")", "+", "(", 9, "/", 3, ")"],
            ),
            6.0,
        )
        self.assertIsInstance(
            tkcalc.evaluate_expression(["(", 1, "+", 1, ")"]),
            int,
        )
        self.assertIsInstance(
            tkcalc.evaluate_expression(["(", 1.0, "+", 1, ")"]),
            float,
        )
        self.assertNotIsInstance(
            tkcalc.evaluate_expression([1, "+", 1]),
            list,
        )
        self.assertNotIsInstance(
            tkcalc.evaluate_expression([1, "+", 1]),
            str,
        )

    def test_evaluate_mult_and_div(self):
        self.assertEqual(
            len(tkcalc.evaluate_mult_and_div([1, "*", 1, "/", -1])),
            1,
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1, "*", 0]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1.1, "*", 0]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1, "/", 1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1, "/", -1]),
            [-1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([-1, "/", -1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([-1, "*", -1, "/", 1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1, "*", -1, "/", -1]),
            [1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([-1, "*", -1, "/", -1]),
            [-1],
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([1, "/", 0]),
            ZeroDivisionError,
        )
        self.assertEqual(
            tkcalc.evaluate_mult_and_div([3, "*", 1, "/", 0]),
            ZeroDivisionError,
        )
        self.assertAlmostEqual(
            tkcalc.evaluate_mult_and_div([1.1, "*", -1.1])[0],
            [-1.21][0],
        )
        self.assertAlmostEqual(
            tkcalc.evaluate_mult_and_div([-1.1, "*", -1.1])[0],
            [1.21][0]
        )
        self.assertIsInstance(
            tkcalc.evaluate_mult_and_div([1, "*", 1]),
            list,
        )
        self.assertIsInstance(
            tkcalc.evaluate_mult_and_div([1, "*", 1])[0],
            int,
        )
        self.assertIsInstance(
            tkcalc.evaluate_mult_and_div([1.0, "/", 1])[0],
            float,
        )
        self.assertNotIsInstance(
            tkcalc.evaluate_mult_and_div([1, "*", 1])[0],
            str,
        )
        self.assertIn(
            "+",
            tkcalc.evaluate_mult_and_div([1, "+", 1]),
        )
        self.assertIn(
            "-",
            tkcalc.evaluate_mult_and_div([1, "-", 1]),
        )
        self.assertIn(
            "²",
            tkcalc.evaluate_mult_and_div([1, "²", 1]),
        )
        self.assertIn(
            "√",
            tkcalc.evaluate_mult_and_div([1, "√", 1]),
        )
        self.assertNotIn(
            "*",
            tkcalc.evaluate_mult_and_div([1, "*", 1, "/", -1]),
        )
        self.assertNotIn(
            "/",
            tkcalc.evaluate_mult_and_div([1, "*", 1, "/", -1]),
        )

    def test_evaluate_percents(self):
        self.assertEqual(
            len(tkcalc.evaluate_percents([1, "%"])),
            1,
        )
        self.assertEqual(
            tkcalc.evaluate_percents([1, "%"])[0],
            0.01,
        )
        self.assertEqual(
            tkcalc.evaluate_percents([-1, "%"])[0],
            -0.01,
        )
        self.assertIsInstance(
            tkcalc.evaluate_percents([1, "%"]),
            list,
        )
        self.assertIsInstance(
            tkcalc.evaluate_percents([1, "%"])[0],
            float,
        )
        self.assertIn(
            "+",
            tkcalc.evaluate_percents([1, "%", "+", 1, "+", -1]),
        )
        self.assertIn(
            "-",
            tkcalc.evaluate_percents([1, "%", "-", 1, "-", -1]),
        )
        self.assertIn(
            "*",
            tkcalc.evaluate_percents([1, "%", "*", 1, "*", -1]),
        )
        self.assertIn(
            "/",
            tkcalc.evaluate_percents([1, "%", "/", 1, "/", -1]),
        )
        self.assertIn(
            "²",
            tkcalc.evaluate_percents([1, "%", "²", 1, "²", -1]),
        )
        self.assertIn(
            "√",
            tkcalc.evaluate_percents([1, "%", "√", 1, "√", -1]),
        )
        self.assertNotIn(
            "%",
            tkcalc.evaluate_percents([1, "%", "+", 1, "/", -1]),
        )

    def test_evaluate_root_and_exp(self):
        self.assertEqual(
            len(tkcalc.evaluate_root_and_exp(["√", 1, "²"])),
            1,
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp([3, "²"]),
            [9],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp([-3, "²"]),
            [9],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["-", 3, "²"]),
            ["-", 9],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp([0, "²"]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["√", 9]),
            [3],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["√", -9]),
            ValueError,
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["√", 9, "²"]),
            [9],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["√", 0]),
            [0],
        )
        self.assertEqual(
            tkcalc.evaluate_root_and_exp(["√", 9, "²"]),
            [9],
        )
        self.assertAlmostEqual(
            tkcalc.evaluate_root_and_exp(["√", 4.84])[0],
            [2.2][0],
        )
        self.assertAlmostEqual(
            tkcalc.evaluate_root_and_exp([2.2, "²"])[0],
            [4.84][0],
        )
        self.assertIsInstance(
            tkcalc.evaluate_root_and_exp([1, "√", 1]),
            list,
        )
        self.assertIsInstance(
            tkcalc.evaluate_root_and_exp([1, "²", 1])[0],
            int,
        )
        self.assertIsInstance(
            tkcalc.evaluate_root_and_exp(["√", 1.0])[0],
            float,
        )
        self.assertNotIsInstance(
            tkcalc.evaluate_root_and_exp([1, "²", 1])[0],
            str,
        )
        self.assertIn(
            "+",
            tkcalc.evaluate_root_and_exp(["²", 1, "+", 1]),
        )
        self.assertIn(
            "-",
            tkcalc.evaluate_root_and_exp(["√", 1, "-", 1]),
        )
        self.assertIn(
            "*",
            tkcalc.evaluate_root_and_exp([1, "²", 1, "*"]),
        )
        self.assertIn(
            "/",
            tkcalc.evaluate_root_and_exp([1, "√", 1, "/", 1]),
        )
        self.assertNotIn(
            "√",
            tkcalc.evaluate_root_and_exp([1, "√", 1, "*", -1]),
        )
        self.assertNotIn(
            "²",
            tkcalc.evaluate_root_and_exp([1, "²", 1, "/", -1]),
        )

    def test_is_valid_syntax(self):
        self.assertTrue(tkcalc.is_valid_syntax(["1"]))
        self.assertTrue(tkcalc.is_valid_syntax(["01.1"]))
        self.assertTrue(tkcalc.is_valid_syntax(["(", "1", ")"]))
        self.assertTrue(tkcalc.is_valid_syntax(["1", "(", "1", ")"]))
        self.assertTrue(tkcalc.is_valid_syntax(["1", "(", "1", ")", "1"]))
        self.assertTrue(tkcalc.is_valid_syntax(["(", "(", "1", ")", ")"]))
        self.assertTrue(tkcalc.is_valid_syntax(["1", "*", "(", "1", ")"]))
        self.assertTrue(tkcalc.is_valid_syntax(["(", "√", "1", ")"]))
        self.assertTrue(tkcalc.is_valid_syntax(["√", "1", "(", "-", "1", ")"]))
        self.assertFalse(tkcalc.is_valid_syntax(["1.1."]))
        self.assertFalse(tkcalc.is_valid_syntax(["1..123"]))
        self.assertFalse(tkcalc.is_valid_syntax(["*", "1"]))
        self.assertFalse(tkcalc.is_valid_syntax(["1", "/"]))
        self.assertFalse(tkcalc.is_valid_syntax(["(", "1", "("]))
        self.assertFalse(tkcalc.is_valid_syntax(["(", "1"]))
        self.assertFalse(tkcalc.is_valid_syntax(["1", ")"]))
        self.assertFalse(tkcalc.is_valid_syntax(["√", "1", "²", "-"]))
        self.assertFalse(tkcalc.is_valid_syntax(["√", "1", "(", "/", "1", ")"]))
        self.assertFalse(tkcalc.is_valid_syntax(["(", "1", "*", "*", "1", ")"]))
        self.assertFalse(tkcalc.is_valid_syntax(["1", "%", "%"]))
        self.assertFalse(tkcalc.is_valid_syntax(["1", "-", "/", "1"]))

    def test_parse_expression(self):
        self.assertEqual(
            tkcalc.parse_expression("⮞   \n"),
            ["0"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   1\n"),
            ["1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   ans\n"),
            ["0"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   123\n"),
            ["123"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   1.23\n"),
            ["1.23"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   1.23+1\n"),
            ["1.23", "+", "1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   1.23/1\n"),
            ["1.23", "/", "1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   .123/1\n"),
            [".123", "/", "1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   .123/.1\n"),
            [".123", "/", ".1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   0.1/.1\n"),
            ["0.1", "/", ".1"],
        )
        self.assertEqual(
            tkcalc.parse_expression("⮞   0.1/*.1\n"),
            ["0.1", "/", "*", ".1"],
        )
        with self.assertRaises(NotImplementedError):
            tkcalc.parse_expression("⮞   0.1~.1\n")

    def test_parse_minuses(self):
        self.assertEqual(
            tkcalc.parse_minuses([1]),
            [1],
        )
        self.assertEqual(
            tkcalc.parse_minuses(["-", 1]),
            [0, "-", 1],
        )
        self.assertEqual(
            tkcalc.parse_minuses(["-", 1, "+", 1]),
            [0, "-", 1, "+", 1],
        )
        self.assertEqual(
            tkcalc.parse_minuses([1, "/", "-", 1]),
            [1, "/", -1],
        )
        self.assertEqual(
            tkcalc.parse_minuses([1, "/", "-", 1.23]),
            [1, "/", -1.23],
        )


if __name__ == "__main__":
    unittest.main()
