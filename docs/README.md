# Compatibility

## Python

| Version | Compatibility | Tested |
| ------- | ------------- | ------ |
| 3.9     | Full          | Yes    |
| 3.8     | Unknown       | No     |
| 3.7     | Unknown       | No     |

## Operating systems

| OS      | Compatibility | Tested | Notes                                        |
| ------- | ------------- | ------ | -------------------------------------------- |
| Linux   | Full          | Yes    | None                                         |
| Windows | Unknown       | No     | No features used are incompatible.           |
| macOS   | Unknown       | No     | Some styling features used are incompatible. |

# Installation

## GUI file manager

To use this application with a GUI file manager:

1. Clone this repository.
2. From within the repository, navigate to the tkcalc directory.
3. Right click on tkcalc.py and choose: Properties > Permissions >
Allow executing file as program
4. Double click on the file and choose 'Run'.

If you are using Nautilus, you must also:

1. Open the three-bar menu in the top-right corner of the window.
2. Choose: Preferences > Behavior > Executable Text Files >
Ask what to do

## Terminal

1. Clone this repository.
2. Run the command `python tkcalc/tkcalc.py` from inside the top-level
directory.

# Help

To view the documentation for this program, clone this repository and
then run the command `python -m pydoc tkcalc/tkcalc.py` from inside
the top-level directory.
