# How to contribute

## Reporting bugs

Bug reports are very welcome. Please report bugs via the
[GitLab issue tracker](https://gitlab.com/emerac/calculator-tkinter/-/issues).

Reports should contain:

1. The expected behavior
2. A brief explanation of the bug
3. The steps necessary to reproduce the bug


## Writing code

This program is not intended to replace a thoroughly-developed calculator.
It is more of a prototype or proof-of-concept and, as such, development
is not expected to continue beyond bug-fixes.

That said, if you would like gain experience collaborating on a low-stakes
project, contributions are welcome.

For a discussion on areas of improvement and the reasons why some design choices
were or were not made, have a look at the [documentation](docs/design-discussion.md).

To facilitate the pull request process, please follow these guidelines:

* Make your changes focused
* Format your code according to [PEP 8](https://www.python.org/dev/peps/pep-0008/).
* Test your code against the preexisting [tests](tests).
* If the current tests do not cover your code, please add new ones that do.

---
**Thank you!**
