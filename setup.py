#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Build script for tkcalc.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import setuptools

with open("README.md", "r", encoding="utf-8") as desc:
    long_description = desc.read()

setuptools.setup(
    name="tkcalc",
    version="0.0.1",
    author="emerac",
    author_email="7785766-emerac@users.noreply.gitlab.com",
    description="A calculator with a tkinter graphic user interface.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GNU General Public License v3 or later (GPLv3+)",
    url="https://gitlab.com/emerac/calculator-tkinter",
    platforms="Linux",
    packages=setuptools.find_packages(exclude=("tests*", "testing*")),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Topic :: Utilities",
    ],
    python_requires=">=3.9",
)
